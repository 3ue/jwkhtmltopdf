jwkhtmltopdf is a Java wrapper to call wkhtmltopdf lib.

```
#!java

package jWkHtmlToPdf.test;

import jWkHtmlToPdf.*;
import jWkHtmlToPdf.WkHtmlToPdf.Colours;
import jWkHtmlToPdf.WkHtmlToPdf.Orientation;
import jWkHtmlToPdf.WkHtmlToPdf.Paperformat;


public class SimpleExample {

	public static void main(){
         String pathToHtml = "input.html";
         String pathToPdf = "output.pdf";
         String pathToLogFile = "log.log";
         float marginLeft = 0;
         float marginRight = 0;
         float marginTop = 0;
         float marginBottom = 0;
         float zoom = 1;

         boolean result = WkHtmlToPdf.convertHtmlToPdf(
        		 pathToHtml,
        		 pathToPdf,
        		 pathToLogFile,
                 marginLeft,
                 marginRight,
                 marginTop,
                 marginBottom,
                 zoom,
                 Colours.COLOURFUL,
                 Paperformat.A4,
        		 Orientation.PORTRAIT);		
	}
}

```