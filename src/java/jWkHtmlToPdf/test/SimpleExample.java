/*
 * Copyright (C) 2016  Uwe Br�nen
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
 */

package jWkHtmlToPdf.test;

import jWkHtmlToPdf.*;
import jWkHtmlToPdf.WkHtmlToPdf.Colours;
import jWkHtmlToPdf.WkHtmlToPdf.Orientation;
import jWkHtmlToPdf.WkHtmlToPdf.Paperformat;


public class SimpleExample {

	public static void main(){
         String pathToHtml = "input.html";
         String pathToPdf = "output.pdf";
         String pathToLogFile = "log.log";
         float marginLeft = 0;
         float marginRight = 0;
         float marginTop = 0;
         float marginBottom = 0;
         float zoom = 1;

         boolean result = WkHtmlToPdf.convertHtmlToPdf(
        		 pathToHtml,
        		 pathToPdf,
        		 pathToLogFile,
                 marginLeft,
                 marginRight,
                 marginTop,
                 marginBottom,
                 zoom,
                 Colours.COLOURFUL,
                 Paperformat.A4,
        		 Orientation.PORTRAIT);		
	}
}
