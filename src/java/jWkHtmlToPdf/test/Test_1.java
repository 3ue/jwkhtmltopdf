/*
 * Copyright (C) 2016  Uwe Br�nen
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
 */

package jWkHtmlToPdf.test;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import jWkHtmlToPdf.WkHtmlToPdf;
import jWkHtmlToPdf.WkHtmlToPdf.Colours;
import jWkHtmlToPdf.WkHtmlToPdf.Orientation;
import jWkHtmlToPdf.WkHtmlToPdf.Paperformat;

public class Test_1 {

    public static void main(String[] args) throws IOException, InterruptedException {
        List<Thread> creators = new ArrayList<>();
        int size = 5;
        for (int i = 0; i < 8; ++i) {
            Creator run = new Creator(i * size, (i + 1) * size);
            Thread t = new Thread(run);
            t.start();
            creators.add(t);
        }

        for (Thread t : creators) {
            t.join();
        }

        System.exit(0);
    }

    private static class Creator implements Runnable {
        int start;
        int end;

        public Creator(int start, int end) {
            this.start = start;
            this.end = end;
        }

        @Override
        public void run() {
            int num_Tables = 2;
            int num_Rows = 10;
            String basic_Path = "Test";
            float marginLeft = 1;
            float marginRight = 1;
            float marginTop = 1;
            float marginBottom = 1;
            float zoom = 1;
            Colours colours = Colours.COLOURFUL;
            Paperformat paperformat = Paperformat.A5;
            Orientation orientation = Orientation.LANDSCAPE;

            for (int i = start; i < end; ++i) {

                try {
                    Test_1.createPDF(
                                     basic_Path,
                                     marginLeft,
                                     marginRight,
                                     marginTop,
                                     marginBottom,
                                     zoom,
                                     colours,
                                     paperformat,
                                     orientation,
                                     num_Tables,
                                     num_Rows,
                                     i);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }

        }

    }

    private static boolean createPDF(
                                     String basic_Path,
                                     float marginLeft,
                                     float marginRight,
                                     float marginTop,
                                     float marginBottom,
                                     float zoom,
                                     Colours colours,
                                     Paperformat paperformat,
                                     Orientation orientation,
                                     int num_Tables,
                                     int num_Rows,
                                     int test)
            throws IOException {
        Path _basic_Path = Paths.get(basic_Path);
        if (!Files.exists(_basic_Path)) {
            Files.createDirectories(_basic_Path);
        }
        Path _log_Path = _basic_Path.resolve("Log_" + String.valueOf(test) + ".log");
        Path _pdf_Path = _basic_Path.resolve("test_" + String.valueOf(test) + ".pdf");

        Path _path_html = create_test_html(
                                           _basic_Path,
                                           num_Rows,
                                           num_Tables,
                                           test,
                                           marginTop,
                                           marginBottom,
                                           marginLeft,
                                           marginRight);

        boolean result = WkHtmlToPdf.convertHtmlToPdf(
                                                      _path_html.toAbsolutePath().toString(),
                                                      _pdf_Path.toAbsolutePath().toString(),
                                                      _log_Path.toAbsolutePath().toString(),
                                                      marginLeft,
                                                      marginRight,
                                                      marginTop,
                                                      marginBottom,
                                                      zoom,
                                                      colours,
                                                      paperformat,
                                                      orientation);

        return result;
    }

    private static Path create_test_html(
                                         Path basic_Path,
                                         int num_Rows,
                                         int num_Tables,
                                         int test,
                                         float mt,
                                         float mb,
                                         float mr,
                                         float ml)
            throws IOException {

        String str_row = MessageFormat.format(
                                              Test_1.row,
                                              String.valueOf(test),
                                              String.valueOf(mt),
                                              String.valueOf(mb),
                                              String.valueOf(ml),
                                              String.valueOf(mr));

        MessageFormat mf_table = new MessageFormat(Test_1.table);

        Path save_path = basic_Path.resolve("Test_" + String.valueOf(test) + ".html");

        StringBuilder tables = new StringBuilder();
        for (int table = 0; table < num_Tables; ++table) {
            StringBuilder rows = new StringBuilder();
            for (int row = 0; row < num_Rows; ++row) {
                rows.append(str_row);
            }
            String args[] = { rows.toString() };
            tables.append(mf_table.format(args));
        }
        StringBuilder sb = new StringBuilder();
        sb.append(page1);
        sb.append(tables);
        sb.append(page2);

        byte text[] = sb.toString().getBytes(UTF_8);
        FileOutputStream fos = new FileOutputStream(save_path.toFile());
        fos.write(text);
        fos.flush();
        fos.close();
        return save_path;
    }

    private static String page1 = "<!DOCTYPE html>\r\n"
            + "<html>\r\n"
            + "<head>\r\n"
            + "<meta charset=\"UTF-8\">\r\n"
            + "<title>Insert title here</title>\r\n"
            + "<style>\r\n"
            + "table, th, td {\r\n"
            + "    border: 1px solid black;\r\n"
            + "}\r\n"
            + "</style>\r\n"
            + "</head>\r\n"
            + "<body>\r\n";
    private static String page2 = "</body>\r\n" + "</html>\r\n";

    private static String table = " <table style=\"border-spacing: 0px; \">\r\n" + "    {0}" + "  </table>\r\n";

    private static String row = "       <tr height=\"16px\">\r\n"
            + "         <td width=\"155px\">\r\n"
            + "             {0}\r\n"
            + "         </td>\r\n"
            + "         <td width=\"155px\">\r\n"
            + "             <span style=\"color: blue;\">{1}</span>\r\n"
            + "         </td>\r\n"
            + "         <td width=\"155px\">\r\n"
            + "             <span style=\"color: red;\">{2}</span>\r\n"
            + "         </td>\r\n"
            + "         <td width=\"155px\">\r\n"
            + "             {3}\r\n"
            + "         </td>\r\n"
            + "         <td width=\"155px\">\r\n"
            + "             {4}\r\n"
            + "         </td>\r\n"
            + "       </tr>\r\n";
}
