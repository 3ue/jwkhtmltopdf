/*
 * Copyright (C) 2016  Uwe Br�nen
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
 */

package jWkHtmlToPdf;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.SynchronousQueue;

public class WkHtmlToPdf {

    static WorkerThread workerThread = new WorkerThread();
    static Thread thread = new Thread(workerThread);

    static {
        try {
            System.loadLibrary("jWkHtmlToPdf");
            System.loadLibrary("wkhtmltox");
        } catch (SecurityException | UnsatisfiedLinkError | NullPointerException e) {
            e.printStackTrace();
        }
        thread.start();
    }

    public enum Colours {
        COLOURFUL("Color"), GRAYSCALE("Grayscale");

        Colours(String type) {
            this.type = type;
        }

        String type;

        String getType() {
            return type;
        }
    };

    public enum Paperformat {
        A0("A0", 841, 1189),
        A1("A1", 594, 841),
        A2("A2", 420, 594),
        A3("A3", 297, 420),
        A4("A4", 210, 297),
        A5("A5", 148, 210),
        A6("A6", 105, 148),
        A7("A7", 74, 105),
        A8("A8", 52, 74),
        A9("A9", 37, 52),
        B0("B0", 1000, 1414),
        B1("B1", 707, 1000),
        B2("B2", 500, 707),
        B3("B3", 353, 500),
        B4("B4", 250, 353),
        B5("B5", 176, 250),
        B6("B6", 125, 176),
        B7("B7", 88, 125),
        B8("B8", 62, 88),
        B9("B9", 33, 62),
        B10("B10", 31, 44),
        C5E("C5E", 163, 229),
        Comm10E("Comm10E", 105, 241),
        DLE("DLE", 110, 220),
        Executive("E,ecutive", 190.5f, 254),
        Folio("Folio", 210, 330),
        Ledger("Ledger", 431.8f, 279.4f),
        Legal("Legal", 215.9f, 355.6f),
        Letter("Letter", 215.9f, 279.4f),
        Tabloid("Tabloid", 279.4f, 431.8f);

        Paperformat(String format, float width, float height) {
            this.format = format;
            this.width = width;
            this.height = height;
        }

        String format;
        float height;
        float width;

        String getFormat() {
            return format;
        }

        float getHeight() {
            return height;
        }

        float getWidth() {
            return width;
        }
    };

    public enum Orientation {
        LANDSCAPE("Landscape"), PORTRAIT("Portrait");
        Orientation(String orientation) {
            this.orientation = orientation;
        }

        String orientation;

        String getOrientation() {
            return orientation;
        }
    }

    public static boolean convertHtmlToPdf(
                                           String pathToHtml,
                                           String pathToPdf,
                                           String pathToLogfile,
                                           float marginLeft,
                                           float marginRight,
                                           float marginTop,
                                           float marginBottom,
                                           float zoom,
                                           Colours colours,
                                           Paperformat paperformat,
                                           Orientation orientation) {
        SynchronousQueue<Boolean> answer = new SynchronousQueue<>();
        WkThread wkThread = new WkThread(
                                         pathToHtml,
                                         pathToPdf,
                                         pathToLogfile,
                                         marginLeft,
                                         marginRight,
                                         marginTop,
                                         marginBottom,
                                         zoom,
                                         colours,
                                         paperformat,
                                         orientation,
                                         answer);

        workerThread.add(wkThread);

        try {
            return answer.take().booleanValue();
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    private static native boolean N_convertHtmlToPdf(
                                                     String pathToHtml,
                                                     String pathToPdf,
                                                     String pathToLogfile,
                                                     float marginLeft,
                                                     float marginRight,
                                                     float marginTop,
                                                     float marginBottom,
                                                     float zoom,
                                                     String colours,
                                                     String orientation,
                                                     float pageWidth,
                                                     float pageHeight);

    private static class WkThread implements Runnable {
        String pathToHtml;
        String pathToPdf;
        String pathToLogfile;
        float marginLeft;
        float marginRight;
        float marginTop;
        float marginBottom;
        float zoom;
        Colours colours;
        Paperformat paperformat;
        Orientation orientation;
        SynchronousQueue<Boolean> answer;

        public WkThread(String pathToHtml, String pathToPdf, String pathToLogfile, float marginLeft, float marginRight,
                float marginTop, float marginBottom, float zoom, Colours colours, Paperformat paperformat,
                Orientation orientation, SynchronousQueue<Boolean> answer) {
            super();
            this.pathToHtml = pathToHtml;
            this.pathToPdf = pathToPdf;
            this.pathToLogfile = pathToLogfile;
            this.marginLeft = marginLeft;
            this.marginRight = marginRight;
            this.marginTop = marginTop;
            this.marginBottom = marginBottom;
            this.zoom = zoom;
            this.colours = colours;
            this.paperformat = paperformat;
            this.orientation = orientation;
            this.answer = answer;
        }

        @Override
        public void run() {
            boolean result = N_convertHtmlToPdf(
                                                pathToHtml,
                                                pathToPdf,
                                                pathToLogfile,
                                                marginLeft,
                                                marginRight,
                                                marginTop,
                                                marginBottom,
                                                zoom,
                                                colours.getType(),
                                                orientation.getOrientation(),
                                                paperformat.getWidth(),
                                                paperformat.getHeight());
            if (answer != null) {
                try {
                    answer.put(new Boolean(result));
                } catch (InterruptedException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    private static class WorkerThread implements Runnable {
        LinkedBlockingQueue<WkThread> inQueue = new LinkedBlockingQueue<>();

        public void add(WkThread wkThread) {
            inQueue.add(wkThread);
        }

        @Override
        public void run() {
            try {
                while (true) {
                    WkThread wkThread = inQueue.take();
                    wkThread.run();
                }
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
}
