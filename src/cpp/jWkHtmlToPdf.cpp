/*
 * Copyright (C) 2016  Uwe Br�nen
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  US
 */

#include "stdafx.h"

#include <string>
#include <sstream>
#include <stdbool.h>
#include <iomanip>
#include <stdio.h>
#include <fstream>
#include <ctime>
#include <chrono>
#include <mutex>
#include <wkhtmltox/pdf.h>


#include "jWkHtmlToPdf_WkHtmlToPdf.h"


using namespace std;

/* Print out loading progress information */
void progress_changed(wkhtmltopdf_converter * c, int p) {
    //printf("%3d%%\r", p);
    //fflush(stdout);
}

/* Print loading phase information */
void phase_changed(wkhtmltopdf_converter * c) {
    //int phase = wkhtmltopdf_current_phase(c);
    //printf("%s\n", wkhtmltopdf_phase_description(c, phase));
}

/* Print a message to stderr when an error occurs */
void error(wkhtmltopdf_converter * c, const char * msg) {
    fprintf(stderr, "Error: %s\n", msg);
}

/* Print a message to stderr when a warning is issued */
void warning(wkhtmltopdf_converter * c, const char * msg) {
    fprintf(stderr, "Warning: %s\n", msg);
}


string float_to_String_cm(float i) {
    stringstream ss;
    ss << fixed << setprecision(2) << i << "cm";
    return ss.str();
}

string float_to_String_mm(float i) {
    stringstream ss;
    ss << fixed << setprecision(1) << i << "mm";
    return ss.str();
}

string float_to_String(float i) {
    stringstream ss;
    ss << fixed << setprecision(2) << i;
    return ss.str();
}

string get_now_as_string() {
    stringstream ss;
    std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
    std::time_t now_c = std::chrono::system_clock::to_time_t(now);
    ss << std::put_time(std::localtime(&now_c), "%F %T");
    return ss.str();
}

mutex convert_mutex;
bool is_Init = false;

void init() {
    if (!is_Init) {
        /* Init wkhtmltopdf in graphics less mode */
        wkhtmltopdf_init(false);
        is_Init = true;
    }
}

JNIEXPORT jboolean JNICALL Java_jWkHtmlToPdf_WkHtmlToPdf_N_1convertHtmlToPdf
(JNIEnv *env, jclass clazz, jstring j_pathToHtml, jstring j_pathToPdf, jstring j_pathToLogfile, jfloat marginLeft, jfloat marginRight, jfloat marginTop, jfloat marginBottom, jfloat zoom, jstring j_colours, jstring j_orientation, jfloat width, jfloat height)
{
    convert_mutex.lock();
    jboolean result = JNI_FALSE;
    const char* pathToHtml = env->GetStringUTFChars(j_pathToHtml, NULL);
    const char* pathToPdf = env->GetStringUTFChars(j_pathToPdf, NULL);
    const char* pathToLogfile;
    if (j_pathToLogfile != NULL) {
        pathToLogfile = env->GetStringUTFChars(j_pathToLogfile, NULL);
    }
    else {
        pathToLogfile = nullptr;
    }

    const char* colours = env->GetStringUTFChars(j_colours, NULL);
    //const char* paperformat = env->GetStringUTFChars(j_paperformat, NULL);
    const char* orientation = env->GetStringUTFChars(j_orientation, NULL);
    
    fstream logFile;

    if (pathToLogfile != nullptr) {
        logFile.open(pathToLogfile, fstream::out | fstream::app);
        logFile << get_now_as_string() << " Start convert File: " << pathToHtml << endl;
    }

    init();

    wkhtmltopdf_global_settings * gs;
    wkhtmltopdf_object_settings * os;
    wkhtmltopdf_converter * c;



    /*
    * Create a global settings object used to store options that are not
    * related to input objects, note that control of this object is parsed to
    * the converter later, which is then responsible for freeing it
    */
    gs = wkhtmltopdf_create_global_settings();
    /* We want the result to be storred in the file called test.pdf */
    wkhtmltopdf_set_global_setting(gs, "out", pathToPdf);
    wkhtmltopdf_set_global_setting(gs, "margin.top", float_to_String_cm(marginTop).c_str());
    wkhtmltopdf_set_global_setting(gs, "margin.bottom", float_to_String_cm(marginBottom).c_str());
    wkhtmltopdf_set_global_setting(gs, "margin.left", float_to_String_cm(marginLeft).c_str());
    wkhtmltopdf_set_global_setting(gs, "margin.right", float_to_String_cm(marginRight).c_str());

    //wkhtmltopdf_set_global_setting(gs, "size.paperSize", "A3");
    wkhtmltopdf_set_global_setting(gs, "size.width", float_to_String_mm(width).c_str());
    wkhtmltopdf_set_global_setting(gs, "size.height", float_to_String_mm(height).c_str());
    wkhtmltopdf_set_global_setting(gs, "orientation", orientation);
    wkhtmltopdf_set_global_setting(gs, "colorMode", colours);


    wkhtmltopdf_set_global_setting(gs, "load.cookieJar", "myjar.jar");
    /*
    * Create a input object settings object that is used to store settings
    * related to a input object, note again that control of this object is parsed to
    * the converter later, which is then responsible for freeing it
    */
    os = wkhtmltopdf_create_object_settings();
    /* We want to convert to convert the qstring documentation page */
    wkhtmltopdf_set_object_setting(os, "page", pathToHtml);
    wkhtmltopdf_set_object_setting(os, "web.enableIntelligentShrinking", "false");
    wkhtmltopdf_set_object_setting(os, "load.zoomFactor", float_to_String(zoom).c_str());

    //wkhtmltopdf_set_object_setting(os, "size.paperSize", paperformat);
    //wkhtmltopdf_set_object_setting(os, "size.paperSize", "A5");
    //wkhtmltopdf_set_object_setting(os, "orientation", orientation);
    //wkhtmltopdf_set_object_setting(os, "colorMode", colours);

    /* Create the actual converter object used to convert the pages */
    c = wkhtmltopdf_create_converter(gs);

    /* Call the progress_changed function when progress changes */
    wkhtmltopdf_set_progress_changed_callback(c, progress_changed);

    /* Call the phase _changed function when the phase changes */
    wkhtmltopdf_set_phase_changed_callback(c, phase_changed);

    /* Call the error function when an error occurs */
    wkhtmltopdf_set_error_callback(c, error);

    /* Call the warning function when a warning is issued */
    wkhtmltopdf_set_warning_callback(c, warning);

    /*
    * Add the the settings object describing the qstring documentation page
    * to the list of pages to convert. Objects are converted in the order in which
    * they are added
    */
    wkhtmltopdf_add_object(c, os, NULL);

    /* Perform the actual conversion */
    if (wkhtmltopdf_convert(c)) {
        if (pathToLogfile != nullptr) {
            logFile << get_now_as_string() << " Successfull convert file: " << pathToHtml << endl;
        }
        result = JNI_TRUE;
    }
    else {
        if (pathToLogfile != nullptr) {
            logFile << get_now_as_string() << " Successfull convert file: " << pathToHtml << endl;
        }
    }

    /* Output possible http error code encountered */
    if (pathToLogfile != nullptr) {
        logFile << get_now_as_string() << " httpErrorCode: " << wkhtmltopdf_http_error_code(c) << endl;
    }

    /* Destroy the converter object since we are done with it */
    wkhtmltopdf_destroy_converter(c);
    wkhtmltopdf_destroy_global_settings(gs);
    wkhtmltopdf_destroy_object_settings(os);

    /* We will no longer be needing wkhtmltopdf funcionality */
    //wkhtmltopdf_deinit();
    


    env->ReleaseStringUTFChars(j_pathToHtml, pathToHtml);
    env->ReleaseStringUTFChars(j_pathToPdf, pathToPdf);
    env->ReleaseStringUTFChars(j_pathToLogfile, pathToLogfile);
    env->ReleaseStringUTFChars(j_colours, colours);
    //env->ReleaseStringUTFChars(j_paperformat, paperformat);
    env->ReleaseStringUTFChars(j_orientation, orientation);


    if (pathToLogfile != nullptr) {
        logFile.close();
    }
    convert_mutex.unlock();
    return result;
}
