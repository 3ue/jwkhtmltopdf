Bevor you can Build the wrapper you have to Download 
the binaries from http://wkhtmltopdf.org/downloads.html.
For the development I use the stable version 0.12.3
Install the Windows version or unzip the file on Linux.


Open the root directory e.g.
	cd Eclipse\jWkHtmlToPdf
Create a new project to build the native lib
	mkdir win32
or
	mkdir win64
or	
	mkdir linux
Open the directory e.g. 
	cd win64
Now let cmake create the project. You need to specify the path to the root directory
from the wkhtmltopdf installation with -DPATH_TO_WKHTMLTOPDF:STRING
For linux call
cmake .. -G "Eclipse CDT4 - Unix Makefiles" -DPATH_TO_WKHTMLTOPDF:STRING="/home/uwe/lib/wkhtmltox/"
For win32 call
"c:\Program Files\CMake\bin\cmake.exe" .. -G "Visual Studio 14" -DPATH_TO_WKHTMLTOPDF:STRING="C:\Program Files (x86)\wkhtmltopdf"
For win64 call
"c:\Program Files\CMake\bin\cmake.exe" .. -G "Visual Studio 14 2015 Win64" -DPATH_TO_WKHTMLTOPDF:STRING="C:\Program Files\wkhtmltopdf"
Open the project and build the lib.